
SLIDES= title archive-vcs manpage demo-placeholder data libavg
SLIDES+= autopatch access-table questions

SLIDEFILES=$(addsuffix .ps, $(SLIDES))

o= >$@.new && mv -f $@.new $@

all:	slides.pdf

libavg-gitk-cut.ppm: libavg-gitk.ppm
	pnmcut -bot 150 <$< $o

libavg.ps: libavg-gitk-cut.ppm libavg-clone.ppm

manpage.ps: dgit.1-manpage Makefile
	man -Tps -l $< >$@.1
	pstops -pa3 '100:0@1.7(-4cm,-47cm)' <$@.1 >$@.2
	ps2pdf -sPAPERSIZE=a3 $@.2 $@.3
	pdfcrop --bbox '0 0 841 595' $@.3 $@.4
	pdf2ps $@.4 $@.5
	pstops -pa4 '0L(21cm,0cm)' <$@.5 >$@

%.ps:   %.fig
	iconv <$< >$@.1 -f UTF-8 -t ISO-8859-1
	# wtf!
	LC_CTYPE=en_GB fig2dev -L ps -l dummy -z A4 <$@.1 $o

data.ps: data-checkout.txt.eps data-dget.txt.eps data-dsc.txt.eps
autopatch.ps: autopatch-top.txt.eps

%.txt.eps: %.txt ./txt2ps
	./txt2ps <$< |ps2eps -s a3 $o

%.ps:	%.lout
	lout $< $o

slides.ps: $(SLIDEFILES) Makefile
	cat $(SLIDEFILES) $o

slides.pdf:     slides.ps Makefile
	ps2pdf $< $@

install: slides.pdf talk.txt
	rsync -vP $^ ijackson@chiark:public-html/2014/debconf-dgit-talk/
	git push ijackson@chiark:public-git/talk-2014-debconf-dgit.git
